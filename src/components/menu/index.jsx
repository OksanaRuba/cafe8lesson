import React, {useState, useEffect} from "react";
import './menu.css';
import Service from '../services';
import {connect} from 'react-redux';
import { addProduct } from '../../redux/actions'

const Menu = ({state, addProduct}) => {

    const menuInf = new Service()  
    const [cart, setCart] = useState(menuInf.getMenu())   

return (
    <>
    {cart === undefined ? <p>Загрузка</p> : <div className='carts'>{cart.filter((e) => {
        return e.stopList === false
    }).map((item, i) => {
        return <div className='cart'>
            <div><img src={item.productImageUrl}></img></div>
            <div>{item.productName}</div>
            <div>{item.price}</div>
            <button onClick={() => {addProduct(item)}}>Купить</button>
        </div>
    })}</div> } 
    </>
)
}

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProduct: (information) => {
            dispatch(addProduct(information))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (Menu);